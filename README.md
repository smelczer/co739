# CO 739


This repository hosts Sage Jupyter notebooks for CO 739 at the University of Waterloo. You can view the notebooks on this repository, download them to run yourself, or (baring technical difficulties) deploy them in the cloud through Binder by clicking on this link.
